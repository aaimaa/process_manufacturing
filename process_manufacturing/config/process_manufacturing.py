from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Production"),
			"items": [
                {
					"type": "doctype",
					"name": "Metal Process Order",
					"description": _("Process Manufacturing Order."),
				},
				{
					"type": "doctype",
					"name": "Stock Entry",
					"description": _("Record item movement."),
				},
				{
					"type": "doctype",
					"name": "Material Request",
					"description": _("Requests for items."),
				},
			]
		},
        {
			"label": _("Process Manufacturing"),
			"items": [
				{
					"type": "doctype",
					"name": "Metal Process",
					"description": _("Process Definition."),
				},
				{
					"type": "doctype",
					"name": "Metal Process Type",
					"description": _("Metal Process Type."),
				},
                {
					"type": "doctype",
					"name": "Metal Manufacturing Department",
					"description": _("Metal Manufacturing Department"),
				},
                {
					"type": "doctype",
					"name": "Item",
					"description": _("All Products or Services."),
				},
                {
					"type": "doctype",
					"name": "Batch",
					"description": _("Batch (lot) of an Item."),
				},
			]
		},
	]
