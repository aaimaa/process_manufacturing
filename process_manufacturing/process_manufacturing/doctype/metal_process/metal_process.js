// Copyright (c) 2017, aaimaa web solutions and contributors
// For license information, please see license.txt

frappe.ui.form.on('Metal Process', {
	refresh: function(frm) {

	},
	setup: function (frm) {
		frm.set_query("workstation", function () {
			return {
				filters: {"metal_department": frm.doc.department}
			}
		});
	}
});
